import styled from "styled-jss";

interface IFilter {
  selected: boolean;
}

export const FILTER = styled("div")({
  borderRadius: "4px",
  width: 60.067,
  height: 68.532,
  display: "flex",
  alignItems: "flex-start",
  justifyContent: "space-between",
  position: "relative",
  background: `url("data:image/svg+xml;charset=utf8, %3Csvg xmlns='http://www.w3.org/2000/svg' width='60.067' height='68.532' viewBox='0 0 60.067 68.532'%3E%3Cpath d='M.034 4a3.999 3.999 0 014-4h52a3.999 3.999 0 014 4v30a3.996 3.996 0 01-2 4l-26 30a4.022 4.022 0 01-4 0l-26-30a3.996 3.996 0 01-2-4z' fill='%23fed60f'/%3E%3C/svg%3E") center no-repeat`,
  transformStyle: "preserve-3d",
  transformOrigin: "center",
  [`&:after`]: {
    content: (props: IFilter) => props.selected === true ? "''" : null,
    position: "absolute",
    // width: "calc(100% + 9.6px + 16px)",
    // height: "calc(100% + 9.6px + 16px)",
    width: 75.518,
    height: 86.89,
    top: "50%",
    left: "50%",
    //border: "4px solid #182e80",      
    animation: "pulse-centered-filter 900ms ease-in-out infinite",
    transformOrigin: "left top",
    background: `url("data:image/svg+xml;charset=utf8, %3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 75.518 86.89'%3E%3Cpath d='M37.016 86.89a6.643 6.643 0 01-6.05-3.833L.613 46.164l-.113-.272a6.549 6.549 0 01-.485-2.968V6.598A6.597 6.597 0 015.908.013L6.136 0h62.21a6.737 6.737 0 014.706 1.918 6.577 6.577 0 011.964 4.672v35.97a6.567 6.567 0 01-.191 5.455 6.737 6.737 0 01-4.05 3.368L43.052 83.087a6.643 6.643 0 01-6.036 3.803zM4.106 44.112l30.317 36.852.115.28a2.689 2.689 0 004.956 0l.126-.309 29.022-33.19.69-.123a2.702 2.702 0 001.923-1.413 2.536 2.536 0 00-.014-2.287l-.225-.434V6.59a2.527 2.527 0 00-.768-1.82A2.713 2.713 0 0068.346 4H6.266a2.602 2.602 0 00-2.25 2.573l-.007 36.593a2.562 2.562 0 00.097.946z' fill='%23fed60f'/%3E%3C/svg%3E") center no-repeat`,
  }
});