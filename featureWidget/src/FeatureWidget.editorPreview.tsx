import { Component, ReactNode, createElement } from "react";

import { FeatureWidgetPreviewProps } from "../typings/FeatureWidgetProps";

declare function require(name: string): string;

export class preview extends Component<FeatureWidgetPreviewProps> {
    render(): ReactNode {
        return (
            <div ref={this.parentInline}>
                <div>test</div>
            </div>
        );
    }

    private parentInline(node?: HTMLElement | null): void {
        // Temporary fix, the web modeler add a containing div, to render inline we need to change it.
        if (node && node.parentElement && node.parentElement.parentElement) {
            node.parentElement.parentElement.style.display = "inline-block";
        }
    }
}

export function getPreviewCss(): string {
    return require("./ui/FeatureWidget.css");
}
