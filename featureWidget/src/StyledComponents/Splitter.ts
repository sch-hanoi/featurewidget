import styled from "styled-jss";

interface ISplitter {
  selected: boolean;
}

export const SPLITTER = styled("div")({
  border: "4.8px solid transparent",
  borderRadius: "4px",
  boxSizing: "border-box",
  width: "80px",
  height: "60px",
  display: "flex",
  alignItems: "flex-start",
  justifyContent: "space-between",
  position: "relative",
  background: `url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='80' height='60' viewBox='0 0 80 60'%3E%3Cpath d='M0,16a4,4,0,0,1,4-4L76,0a4,4,0,0,1,4,4V56a4,4,0,0,1-4,4L4,48a4,4,0,0,1-4-4Z' fill='%23182e80'/%3E%3C/svg%3E") center no-repeat`,
  [`&:after`]: {
    content: (props: ISplitter) => props.selected === true ? "''" : null,
    position: "absolute",
    width: "calc(100% + 9.6px + 16px)",
    height: "calc(100% + 9.6px + 16px)",
    //border: "4px solid #182e80",
    left: -12.8,
    top: -12.8,
    animation: "pulse 900ms ease-in-out infinite",
    background: `url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='94.0009' height='75' viewBox='0 0 94.0009 75'%3E%3Cpath d='M87.50045,75l-.341-.029L6.31246,61a6.6,6.6,0,0,1-6.312-6.67V20.67A6.6,6.6,0,0,1,6.31246,14l81.017-14h.171a6.594,6.594,0,0,1,6.5,6.67V68.33a6.67408,6.67408,0,0,1-1.889,4.7A6.419,6.419,0,0,1,87.50045,75Zm-81-57a2.593,2.593,0,0,0-2.5,2.67V54.33a2.593,2.593,0,0,0,2.5,2.67l.341.029L87.64544,71a2.42688,2.42688,0,0,0,1.589-.747,2.74808,2.74808,0,0,0,.766-1.923V6.67A2.6,2.6,0,0,0,87.64645,4L6.67144,18Z' fill='%23182e80'/%3E%3C/svg%3E") center no-repeat`
  }

});