/**
 * This file was generated from FeatureWidget.xml
 * WARNING: All changes made to this file will be overwritten
 * @author Mendix UI Content Team
 */
import { ComponentType, CSSProperties, ReactNode } from "react";
import { ActionValue, DynamicValue, EditableValue, ListValue, ListActionValue, ListAttributeValue, ListWidgetValue } from "mendix";

export interface EmployeeContextMenuType {
    isDefault: boolean;
    title: string;
    onClickMf?: ActionValue;
    isVisible: DynamicValue<boolean>;
}

export interface EmployeeContextMenuPreviewType {
    isDefault: boolean;
    title: string;
    onClickMf: {} | null;
    isVisible: string;
}

export interface FeatureWidgetContainerProps {
    name: string;
    class: string;
    style?: CSSProperties;
    tabIndex?: number;
    mode: EditableValue<boolean>;
    modeColor: DynamicValue<string>;
    highlightedEmployeeDs: ListValue;
    companyDs: ListValue;
    employeeDs: ListValue;
    companyName: ListAttributeValue<string | BigJs.Big>;
    openCompany?: ListActionValue;
    employeeName: ListAttributeValue<string>;
    employeeAge: ListAttributeValue<BigJs.Big>;
    openEmployee?: ListActionValue;
    selectEmployee?: ListActionValue;
    content?: ListWidgetValue;
    getOtherObjects?: ListValue;
    content2?: ReactNode;
    showEmployeeContextMenu: DynamicValue<boolean>;
    employeeContextMenu: EmployeeContextMenuType[];
}

export interface FeatureWidgetPreviewProps {
    class: string;
    style: string;
    mode: string;
    modeColor: string;
    highlightedEmployeeDs: {} | null;
    companyDs: {} | null;
    employeeDs: {} | null;
    companyName: string;
    openCompany: {} | null;
    employeeName: string;
    employeeAge: string;
    openEmployee: {} | null;
    selectEmployee: {} | null;
    content: { widgetCount: number; renderer: ComponentType };
    getOtherObjects: {} | null;
    content2: { widgetCount: number; renderer: ComponentType };
    showEmployeeContextMenu: string;
    employeeContextMenu: EmployeeContextMenuPreviewType[];
}
