import styled from "styled-jss";

interface IOutboundChannelAdapter {
  selected: boolean;
}

export const OUTBOUNDCHANNELADAPTER = styled("div")({
  borderRadius: "60px",
  backgroundColor: "#FFF",
  border: "4.8px solid #69b731",
  boxSizing: "border-box",
  width: "60px",
  height: "60px",
  display: "flex",
  alignItems: "flex-start",
  justifyContent: "space-between",
  position: "relative",

  [`&:after`]: {
    content: (props: IOutboundChannelAdapter) => props.selected === true ? "''" : null,
    position: "absolute",
    width: "calc(100% + 9.6px + 16px)",
    height: "calc(100% + 9.6px + 16px)",
    border: "4px solid #69b731",
    borderRadius: "60px",
    boxSizing: "border-box",
    left: -12.8,
    top: -12.8,
    animation: "pulse 900ms ease-in-out infinite"
  }

});