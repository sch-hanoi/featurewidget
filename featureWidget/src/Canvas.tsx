import { FlowElement, Position, default as ReactFlow } from "react-flow-renderer";
import { InboundAdapter, default as InboundChannelAdapterComponent } from "./components/InboundChannelAdapter";
import { Filter, default as FilterComponent } from "./components/Filter";
import { Transformer, default as TransformerComponent } from "./components/Transformer";

import { createElement } from "react";

const NodeTypes = { InboundChannelAdapterComponent, FilterComponent, TransformerComponent }
type AllType = keyof typeof NodeTypes;
type OverwriteType<T> = {
    [key in keyof T]: key extends "type" ? AllType : T[key];
}
type EmagizFlowElement<T> = OverwriteType<FlowElement<T>>;

const Canvas = () => {
    const inboundAdapter: EmagizFlowElement<InboundAdapter> = {
        id: '1', data: { text: 'Node 1' },
        type: "InboundChannelAdapterComponent",
        position: { x: 50, y: 50 }
    };
    const transformer: EmagizFlowElement<Transformer> = {
        id: '2', data: { text: 'Node 2' },
        type: "TransformerComponent",
        position: { x: 250, y: 50 }
    };
    const filter: EmagizFlowElement<Filter> = {
        id: '3', data: { text: 'Node 3' },
        type: "FilterComponent",
        targetPosition: Position.Left,
        sourcePosition: Position.Right,
        position: { x: 550, y: 50 }
    };

    const elements = [
        inboundAdapter,
        transformer,
        filter,
        // you can also pass a React component as a label
        { id: 'e1-2', source: '1', target: '2', animated: true },
    ];
    return (
        <div style={{ width: "100%", height: 400 }}>
            <ReactFlow
                nodeTypes={NodeTypes}
                elements={elements} />

        </div>
    )
}
export default Canvas;