import styled from "styled-jss";

interface IInboundChannelAdapter {
  selected: boolean;
}

export const INBOUNDCHANNELADAPTER = styled("div")({
  borderRadius: "4px",
  width: 60,
  height: 60,
  display: "flex",
  alignItems: "flex-start",
  justifyContent: "space-between",
  position: "relative",
  background: `url("data:image/svg+xml;charset=utf8, %3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 60 60'%3E%3Ccircle cx='30' cy='30' r='30' fill='%2369b731'/%3E%3C/svg%3E") center no-repeat`,
  [`&:after`]: {
    content: (props: IInboundChannelAdapter) => props.selected === true ? "''" : null,
    position: "absolute",
    // width: "calc(100% + 9.6px + 16px)",
    // height: "calc(100% + 9.6px + 16px)",
    width: 75,
    height: 75,
    top: "50%",
    left: "50%",
    //border: "4px solid #182e80",      
    animation: "pulse-centered 900ms ease-in-out infinite",
    transformOrigin: "left top",
    background: `url("data:image/svg+xml;charset=utf8, %3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 75 75'%3E%3Cpath d='M37.5 75A37.5 37.5 0 1175 37.5 37.542 37.542 0 0137.5 75zm0-71A33.5 33.5 0 1071 37.5 33.538 33.538 0 0037.5 4z' fill='%2369b731'/%3E%3C/svg%3E") center no-repeat`,
  }
});