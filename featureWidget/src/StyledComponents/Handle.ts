import { Handle } from "react-flow-renderer";
import styled from "styled-jss";
export interface IPort {
  selected: boolean;
  type: string;
  position: string;
}
export const HANDLE = styled(Handle)({
  position: "absolute",
  display: "flex",
  zIndex: 3,

  // left: (props: IPort) => props.position === "left" ? 0 : "auto",
  top: (props: { position: string }) => {
    switch (props.position) {
      case "top":
        return 0;
      case "bottom":
        return "auto";
      default:
        return "50%";
    }
  },
  right: (props: { position: string }) => {
    switch (props.position) {
      case "right":
        return 0;
      default:
        return "auto";
    }
  },
  bottom: (props: { position: string }) => {
    switch (props.position) {
      case "bottom":
        return 0;
      case "top":
        return "auto";
      default:
        return "50%";
    }
  },
  left: (props: { position: string }) => {
    switch (props.position) {
      case "left":
        return 0;
      case "right":
        return "auto";
      default:
        return "50%";
    }
  },
  // right: (props: IPort) => props.position === "right" ? 0 : "auto",

  // bottom: (props: IPort) => props.position === "bottom" ? 0 :"auto",
  // transform: (props: IPort) => props.type === "source" ? "translateX(-50%) translateY(-50%)" : "translateX(50%) translateY(-50%)",
  transform: (props: { position: string }) => {
    switch (props.position) {
      case "top":
        return "translateX(-50%) translateY(-50%)";
      case "right":
        return "translateX(50%) translateY(-50%)";
      case "bottom":
        return "translateX(-50%) translateY(50%)";
      case "left":
        return "translateX(-50%) translateY(-50%)";
      default:
        return "translateX(-50%) translateY(-50%)";
    }
  },
  [`&:after`]: {
    content: '""',
    background: (props: { type: string }) => {
      switch (props.type) {
        case "source":
          return `url("data:image/svg+xml;charset=utf8, %3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 14.4 14.4'%3E%3Ccircle cx='7.2' cy='7.2' r='6' fill='%238f8f8f'/%3E%3Cpath d='M7.2 14.4a7.2 7.2 0 117.2-7.2 7.208 7.208 0 01-7.2 7.2zm0-12A4.8 4.8 0 1012 7.2a4.805 4.805 0 00-4.8-4.8z' fill='%23fff'/%3E%3C/svg%3E") center no-repeat`;
        case "error":
          return `url("data:image/svg+xml;charset=utf8, %3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 14.4 14.4'%3E%3Ccircle cx='7.2' cy='7.2' r='6' fill='%23eb1b2b'/%3E%3Cpath d='M7.2 14.4a7.2 7.2 0 117.2-7.2 7.208 7.208 0 01-7.2 7.2zm0-12A4.8 4.8 0 1012 7.2a4.805 4.805 0 00-4.8-4.8z' fill='%23fff'/%3E%3C/svg%3E") center no-repeat`;
        case "target":
          return `url("data:image/svg+xml;charset=utf8, %3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 15.883 15.883'%3E%3Cpath d='M1.2 1.942v12l12-6z' fill='%238f8f8f'/%3E%3Cpath d='M0 15.883V0l15.883 7.941zm2.4-12V12l8.117-4.059z' fill='%23fff'/%3E%3C/svg%3E") center no-repeat`;
        case "filter":
          return `url("data:image/svg+xml;charset=utf8, %3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 15.395 15.395'%3E%3Cpath d='M7.697 1.697l6 6-6 6-6-6z' fill='%238f8f8f'/%3E%3Cpath d='M7.697 15.395L0 7.697 7.697 0l7.698 7.697zM3.395 7.697L7.697 12 12 7.697 7.697 3.395z' fill='%23fff'/%3E%3C/svg%3E") center no-repeat`;
        default:
          return "none";
      }
    },
    margin: "auto",
    // width: (props: IPort) => props.type === "target" ? 15.883 : 14.4,
    height: (props: { type: string }) => {
      switch (props.type) {
        case "source":
          return 14.4;
        case "error":
          return 14.4;
        case "target":
          return 15.883;
        case "filter":
          return 15.395;
        default:
          return "none";
      }
    },
    width: (props: { type: string }) => {
      switch (props.type) {
        case "source":
          return 14.4;
        case "error":
          return 14.4;
        case "target":
          return 15.883;
        case "filter":
          return 15.395;
        default:
          return "none";
      }
    },
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)"
  }
});