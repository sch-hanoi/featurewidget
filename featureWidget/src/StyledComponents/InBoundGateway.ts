import { HANDLE } from "./Handle";
import styled from "styled-jss";

interface IInboundGateway {
  selected: boolean;
}

export const INBOUNDGATEWAY = styled("div")({
  border: "4.8px solid transparent",
  borderRadius: "60px",
  backgroundColor: "#69b731",
  boxSizing: "border-box",
  width: "60px",
  height: "180px",
  display: "flex",
  alignItems: "flex-start",
  justifyContent: "space-between",
  position: "relative",
  // animation: (props: IInboundGateway) => props.selected === true ? "pulse-element 1.25s cubic-bezier(0.215, 0.61, 0.355, 1) infinite": 'none',

  [`&:after`]: {
    content: (props: IInboundGateway) => props.selected === true ? "''" : null,
    position: "absolute",
    width: "calc(100% + 9.6px + 16px)",
    height: "calc(100% + 9.6px + 16px)",
    border: "4px solid #69b731",
    borderRadius: "60px",
    boxSizing: "border-box",
    left: -12.8,
    top: -12.8,
    animation: "pulse 900ms ease-in-out infinite"
  },
  [`& ${HANDLE}`]: {
    left: "auto",
    right: -4.8 / 2,
    top: (-4.8 / 2) + 40,
    transform: "translateX(50%) translateY(-50%)",
    [`&:first-child`]: {
      top: "calc(100% - 40px)",
      //bottom: 40.8,
      transform: "translateX(50%) translateY(-50%) scaleX(-1)"
    }
  },

});