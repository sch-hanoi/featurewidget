import { HANDLE } from "../StyledComponents/Handle";
import { INBOUNDCHANNELADAPTER } from "../StyledComponents/InBoundChannelAdapter";
import { Position } from "react-flow-renderer";
import { createElement } from "react";

export interface InboundAdapter {
    text: string;
    selected?: boolean;
}
const InboundChannelAdapter = (data: InboundAdapter) => {
    return (
        <INBOUNDCHANNELADAPTER selected={data.selected}>
            <div>{data.text}</div>
            <HANDLE
                type="source"
                position={Position.Right}
                id="random_id_here"
            />
            <HANDLE
                type="error"
                position={Position.Bottom}
                id="random_id_here_too"
            />

        </INBOUNDCHANNELADAPTER>
    );
};
export default InboundChannelAdapter;