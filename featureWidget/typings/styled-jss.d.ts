interface createStyle { (style: Object): React.ReactComponentElement };
declare module 'styled-jss' {
    function styled(tag: string | React.ReactComponentElement): createStyle;
    export default styled;
}