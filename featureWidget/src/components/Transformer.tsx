import { HANDLE } from "../StyledComponents/Handle";
import { Position } from "react-flow-renderer";
import { TRANSFORMER } from "../StyledComponents/Transformer";
import { createElement } from "react";

export interface Transformer {
    text: string;
    selected?: boolean;
}
const Transformer = (data: Transformer) => {
    return (
        <TRANSFORMER selected={data.selected}>
            <div>{data.text}</div>

            <HANDLE
                type="target"
                position={Position.Left}
                id="random_id_here"
            />
            <HANDLE
                type="source"
                position={Position.Right}
                id="random_id_here_too"
            />

        </TRANSFORMER>
    );
};
export default Transformer;