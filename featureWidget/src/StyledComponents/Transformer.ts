import styled from "styled-jss";

interface ITransformer {
  selected: boolean;
}

export const TRANSFORMER = styled("div")({
  borderRadius: "4px",
  width: 80,
  height: 60,
  display: "flex",
  alignItems: "flex-start",
  justifyContent: "space-between",
  position: "relative",
  background: `url("data:image/svg+xml;charset=utf8, %3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 80 60'%3E%3Cpath d='M0 4a3.999 3.999 0 014-4h72a3.999 3.999 0 014 4v52a3.999 3.999 0 01-4 4H4a3.999 3.999 0 01-4-4z' fill='%23182e80'/%3E%3C/svg%3E") center no-repeat`,
  transformStyle: "preserve-3d",
  transformOrigin: "center",
  [`&:after`]: {
    content: (props: ITransformer) => props.selected === true ? "''" : null,
    position: "absolute",
    width: 94,
    height: 74,
    top: "50%",
    left: "50%",
    animation: "pulse-centered 900ms ease-in-out infinite",
    transformOrigin: "left top",
    background: `url("data:image/svg+xml;charset=utf8, %3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 94 74'%3E%3Cpath d='M87.5 74h-81a6.429 6.429 0 01-4.627-1.99A6.657 6.657 0 010 67.33V6.67A6.594 6.594 0 016.5 0h81A6.594 6.594 0 0194 6.67v60.66a6.672 6.672 0 01-1.89 4.698A6.413 6.413 0 0187.5 74zM6.5 4A2.592 2.592 0 004 6.67v60.66a2.732 2.732 0 00.75 1.902A2.426 2.426 0 006.5 70h81a2.41 2.41 0 001.732-.75A2.745 2.745 0 0090 67.33V6.67A2.592 2.592 0 0087.5 4z' fill='%23182e80'/%3E%3C/svg%3E") center no-repeat`,
  }
});