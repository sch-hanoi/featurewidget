/// <reference path="../typings/styled-jss.d.ts" />
import "./ui/FeatureWidget.css";
import "./ui/animations.css";

import { ObjectItem, ValueStatus } from "mendix";
import { ReactNode, createElement } from "react";

import Canvas from "./Canvas";
import { FeatureWidgetContainerProps } from "../typings/FeatureWidgetProps";
import { TITLE } from "./components/Label";

function FeatureWidget(props: FeatureWidgetContainerProps): ReactNode {
    const openCompany = (_company: ObjectItem) => {
        props.openCompany && props.openCompany(_company);
    };
    const highlighedEmployees = props.highlightedEmployeeDs.items;

    const isHighlightedEmployee = (_emp: ObjectItem) => {
        return highlighedEmployees?.length && highlighedEmployees.map(item => item.id).includes(_emp.id);
    }
    const company = (props.companyDs.status === ValueStatus.Available)
        && props.companyDs.items![0];

    return (
        <div >
            <Canvas />
            <TITLE>Company</TITLE>
            {
                company ?
                    <div style={{ backgroundColor: props.modeColor.value }}
                        onDoubleClick={() => openCompany(company)}>
                        {props.companyName(company).displayValue}
                        {props.mode.value && <div>Editting...</div>}
                    </div>
                    :
                    <div>No company</div>
            }

            <div className="row">
                <div className="col-8">
                    <ol>
                        {props.employeeDs?.items?.map((item: ObjectItem) => {
                            const isHighlighted = isHighlightedEmployee(item);
                            return <li
                                key={item.id}
                                style={{ backgroundColor: isHighlighted ? "yellow" : "white" }}
                                onClick={() => props.selectEmployee && props.selectEmployee(item).execute()}
                                onDoubleClick={() => props.openEmployee && props.openEmployee(item).execute()}>
                                {props.employeeName(item).displayValue}
                                {isHighlighted ? "---> in the editing departement" : ""}
                            </li>
                        })}
                    </ol>
                </div>
                {
                    props.showEmployeeContextMenu.value ?
                        <div className="col-4" style={{ backgroundColor: "yellowgreen" }}>
                            {
                                props.employeeContextMenu.map(item =>
                                    item.isVisible.value
                                    && <div
                                        onClick={() => item.onClickMf && item.onClickMf.execute()}
                                    >
                                        {item.title}
                                    </div>
                                )
                            }
                        </div>
                        : <div />
                }
            </div>
            {
                props.getOtherObjects && props.content && <div>Plug widgets: Click to edit Department</div>
            }
            {
                props.getOtherObjects?.items?.map((item: ObjectItem) =>
                    props.content && props.content(item)
                )
            }
            {
                props.content2
            }
        </div>
    );
}
export default FeatureWidget;
