import styled from "styled-jss";

interface IFilter {
  selected: boolean;
}

export const FILTERHTML = styled("div")({
  display: "grid",
  gridTemplateAreas: "'top' 'bottom'",
  position: "relative",
  width: 60,
  height: 68,
  border: "4.8px solid transparent",
  boxSizing: "border-box",
  [`& .top`]: {
    //border: "4.8px solid transparent",
    borderRadius: "4px",
    backgroundColor: "#fed60f",
    boxSizing: "border-box",
    width: "60px",
    height: "38px",
    gridArea: "top",
    position: "relative",
    transformStyle: "preserve-3d",
    zIndex: 2,
    margin: [-4.8, -4.8, 0, -4.8],
    [`&:before`]: {
      content: (props: IFilter) => props.selected === true ? "''" : null,
      position: "absolute",
      width: "calc(100% + 16px)",
      height: "calc(100% + 16px)",
      border: "4px solid #fed60f",
      borderBottom: "none",
      background: "linear-gradient(to bottom, #fff 81%, rgba(0,0,0,0) 81%);",
      borderRadius: "8px",
      boxSizing: "border-box",
      left: -8,
      top: -8,
      zIndex: -1,
      transform: "translateZ(-1em)",
      animation: "pulse 900ms ease-in-out infinite"
    }
  },
  [`& .bottom`]: {
    //border: "4.8px solid transparent",
    borderRadius: "4px",
    backgroundColor: "#fed60f",
    boxSizing: "border-box",
    left: (60 - 42.42640687119285) / 2,
    top: 42.42640687119285 / -2,
    width: 42.42640687119285,
    height: 42.42640687119285,
    gridArea: "bottom",
    transformStyle: "preserve-3d",
    transformOrigin: "center",
    transform: "rotate(45deg)",
    position: "relative",
    zIndex: 1,
    margin: [0, -4.8, -4.8, -4.8],


    [`&:before`]: {
      content: (props: IFilter) => props.selected === true ? "''" : null,
      position: "absolute",
      width: "calc(100% + 16px)",
      height: "calc(100% + 16px)",
      border: "4px solid #fed60f",
      borderRadius: "8px",
      boxSizing: "border-box",
      left: -8,
      top: -8,
      animation: "pulse 900ms ease-in-out infinite",
      transform: "translateZ(-1em)",
    }
  },


});