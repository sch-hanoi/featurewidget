import { FILTER } from "../StyledComponents/Filter";
import { HANDLE } from "../StyledComponents/Handle";
import { Position } from "react-flow-renderer";
import { createElement } from "react";

export interface Filter {
    text: string;
    selected?: boolean;
}
const Filter = (data: Filter) => {
    return (
        <FILTER selected={data.selected}>
            <HANDLE
                type="source"
                position={Position.Right}
                id="random_id_here"
            />
            <HANDLE
                type="filter"
                position={Position.Bottom}
                id="random_id_here_too"
            />
            <HANDLE
                type="target"
                position={Position.Left}
                id="random_id_here"
            />
        </FILTER>
    );
};
export default Filter;